const std = @import("std");
const builtin = @import("builtin");
const datetime = @import("datetime/root.zig");

const save_file = "save.data";


const id_length: usize = 5;
const magic_id_number: u32 = 7343095;

const Mapper_Hash = struct {
    map: std.StringHashMap([]const []const u8),

    pub fn get(self: Mapper_Hash, key: []const u8) ?[]const []const u8 {
        return self.map.get(key);
    }
};

const Mapper_Slice = struct {
    data: []const Item,

    const Item = struct {
        key: []const u8,
        value: []const []const u8,
    };

    pub fn get(self: Mapper_Slice, key: []const u8) ?[]const []const u8 {
        for (self.data) |d| {
            if (std.mem.eql(u8, d.key, key)) return d.value;
        }
        return null;
    }
};

const Mapper_Single = struct {
    key: []const u8,
    value: []const []const u8,

    pub fn get(self: Mapper_Single, key: []const u8) ?[]const []const u8 {
        if (!std.mem.eql(u8, key, self.key)) return null;
        return self.value;
    }
};

const Key_Not_Found_Error = error {
    key_not_found,
};

fn replace(text: []const u8, sub: anytype, pieces: *std.ArrayList([]const u8)) !void {
    var section_beginning: usize = 0;
    var cursor:            usize = 0;

    while (cursor < text.len) : (cursor += 1) {
        const c = text[cursor];

        if (c != '{') continue; 
        
        cursor += 1;
        if (cursor < text.len and text[cursor] == '{') continue;

        const end = blk: {
            for (text[cursor..text.len], cursor..) |d, it| {
                if (d == '}') break :blk it;
            } else {
                unreachable;
            }
        };

        const key = text[cursor..end];
        const value = sub.get(key) orelse {
            std.debug.print("Key not found: `{s}`\n", .{key});
            return error.key_not_found;
        };
        try pieces.append(text[section_beginning..cursor-1]);
        try pieces.appendSlice(value);

        cursor = end;
        section_beginning = cursor + 1;
    }

    if (section_beginning < text.len) {
        try pieces.append(text[section_beginning..text.len]);
    }
}

fn get_id(seed: u32) [id_length]u8 {
    const n = comptime blk: {
        var i = 1;
        for (0..id_length) |_| {
            i *= 26;
        }
        break :blk i;
    };

    var val: [id_length]u8 = [1]u8{'a'} ** id_length;
    var t = (@as(u64, @intCast(seed )) * @as(u64, @intCast(magic_id_number))) % n;
    var i: usize = 0;
    while (t > 0) {
        val[i] = 'a' + @as(u8, @intCast((t % 26)));
        t = t / 26;
        i += 1; 
    }
    return val;
}

fn parse_id(id: [id_length]u8) u32 {
    const inverse_k, const n = comptime blk: {
        const n, var phi = blk2: {
            var i: u64 = 1;
            for (1..id_length) |_| {
                i *= 26;
            }
            const z = i * 12;
            i *= 26;
            break :blk2 .{i, z-1};
        };
        var k: u64 = @intCast(magic_id_number);
        var i: u64 = 1; 
        while (phi > 0) {
            if (phi % 2 == 1) i = (i * k) % n;
            k = (k * k) % n;
            phi = phi / 2;
        }
        break :blk .{i, n};
    };

    var iter = std.mem.reverseIterator(&id);
    var parsed: u32 = 0;
    while (iter.next()) |c| {
        const v: u32 = @intCast(c - 'a');
        parsed = 26 * parsed + v;
    }
    const res: u32 = @intCast((inverse_k * @as(u64, parsed)) % n);
    return res;
}

test "id" {
    for (0..30) |it| {
        const id = get_id(@intCast(it));
        const rec = parse_id(id);
        errdefer {
            std.debug.print("id is {s}\n", .{&id});
        }
        try std.testing.expectEqual(it, rec);
    }
}

const New_Entry_Data = struct {
    page_number:       []const u8,
    input_entry_title: []const u8,
    input_source_link: []const u8,
    input_tags:        []const u8,
    input_note:        []const u8,
};

const Data_Entry = struct {
    page_number:   []const u8,
    entry_title:   []const u8,
    source_link:   []const u8,
    note:          []const u8,
    creation_date: []const u8,
    id:            u32,
    tags:          [][]const u8,
};

const Counter_Writer = struct {
    counter: usize = 0,
    child: std.io.AnyWriter,

    fn _write(context: *const anyopaque, bytes: []const u8) !usize {
        const ptr: *Counter_Writer = @constCast(@alignCast(@ptrCast(context)));
        return ptr.write(bytes);
    }
    
    pub inline fn write(self: *Counter_Writer, bytes: []const u8) !usize {
        self.counter += bytes.len;
        return self.child.write(bytes);
    }

    pub fn any(self: *Counter_Writer) std.io.AnyWriter {
        return .{
            .context = @ptrCast(self),
            .writeFn = _write,
        };
    }
};

const Event_Manager = struct {
    events: std.ArrayList(Event),
};

const Event = union(enum) {
    Entry_Insert: struct {
        pos: u32,
        id: u32,
    },
    Id_Generator: u32,
    Index_Position: u32,
};

const Serialize_Action = enum {
    skip,
    serialize,
};

fn _serialize(comptime T: type, x: T, writer: std.io.AnyWriter, event_manager: *Event_Manager) !void {
    const ty = @typeInfo(T);
    switch (ty) {
        .Bool => try writer.writeByte(if (x) 1 else 0),
        .Int => try writer.writeInt(T, x, .little),
        .Array =>  {
            for (&x) |y| {
                try _serialize(ty.Array.child, y, writer, event_manager);
            }
        },
        .Pointer => {
            std.debug.assert(ty.Pointer.size == .Slice);
            try writer.writeInt(usize, x.len, .little);
            const ty_child = @typeInfo(ty.Pointer.child);
            const is_child_simple = ty_child == .Int and ty_child.Int.bits == 8 or ty_child == .Bool;
            if (is_child_simple) {
                const slice = std.mem.sliceAsBytes(x);
                _ = try writer.write(slice);
            } else {
                for (x) |y| {
                    try _serialize(ty.Pointer.child, y, writer, event_manager);
                }
            }
        },
        .Struct => {
            const has_pre_hook = blk: {
                inline for (ty.Struct.decls) |d| {
                    const name_len = std.mem.indexOfSentinel(u8, 0, d.name);
                    if (std.mem.eql(u8, d.name[0..name_len], "serialize_pre_hook")) {
                        break :blk true;
                    }
                }
                break :blk false;
            };
            comptime var action = .serialize;
            if (has_pre_hook) {
                action = try x.serialize_pre_hook(writer, event_manager);
            }
            if (action == .serialize) {
                inline for (ty.Struct.fields) |f| {
                    try _serialize(f.type, @field(x, f.name), writer, event_manager);
                }
            }
        },
        else => std.debug.panic("Type {} cannot be serialized", .{T}),
    }
}

fn serialize(comptime T: type, x: T, writer: std.io.AnyWriter, event_manager: *Event_Manager) !void {
    var counter_writer = Counter_Writer {
        .counter = 0,
        .child =  writer,
    };
    try _serialize(T, x, counter_writer.any(), event_manager);
}

fn deserialize(comptime T: type, reader: std.io.AnyReader, allocator: std.mem.Allocator) !T {
    const ty = @typeInfo(T);
    switch (ty) {
        .Bool => {
            const v = try reader.readByte();
            return v != 0;
        },
        .Int => {
            return try reader.readInt(T, .little);
        },
        .Array =>  {
            var v: T = undefined;
            for (0..ty.Array.len) |it| {
                v[it] = try deserialize(ty.Array.child, reader, allocator);
            }
            return v;
        },
        .Pointer => {
            const len = try reader.readInt(usize, .little);
            const v = try allocator.alloc(ty.Pointer.child, len);
            const ty_child = @typeInfo(ty.Pointer.child);
            const is_child_simple = ty_child == .Int and ty_child.Int.bits == 8 or ty_child == .Bool;
            if (is_child_simple) {
                const buffer: []u8 = @ptrCast(v);
                const size = try reader.read(buffer);
                std.debug.assert(size == buffer.len);
            } else {
                for (v) |*it| {
                    it.* = try deserialize(ty.Pointer.child, reader, allocator);
                }
            }
            return v;
        },
        .Struct => {
            var v: T = undefined;
            inline for (ty.Struct.fields) |f| {
                @field(v, f.name) = try deserialize(f.type, reader, allocator);
            }       
            return v;
        },
        else => std.debug.panic("Type {} cannot be serialized", .{T}),
    }
}


const Save_Structure = struct {
    id_generator:     u64,
    footer_pointer:   u64,
    inactive_entries: u64,
    _reserved_space:  [104]u8,
    data: []Save_Entry, 

    const Save_Entry = struct {
        is_active: bool,
        entry: Data_Entry,

        inline fn serialize_pre_hook(self: Save_Entry, writer: std.io.AnyWriter, event_manager: *Event_Manager) !Serialize_Action {
            const counter_writer: *Counter_Writer = @ptrCast(writer.context);
            const event = Event {
                .Entry_Insert = .{
                    .id = self.entry.id,
                    .pos = counter_writer.counter,
                }
            };
            try event_manager.events.append(event);
            return .serialize;
        }
    };

};

const Save_Manager = struct {
    file_path: []const u8,
    save: Save_Structure,
    new_entries: std.ArrayList(Data_Entry),
    allocator: std.mem.Allocator,

    pub fn update_save(self: *Save_Manager) !void {
        const new_len = self.save.data.len + self.new_entries.items.len - @as(usize, @intCast(self.save.inactive_entries));
        const new_data = try self.new_entries.allocator.alloc(Save_Structure.Save_Entry, new_len);

        var cursor: usize = 0;
        for (self.save.data) |e| {
            if (e.is_active) {
                new_data[cursor] = e;
                cursor += 1;
            }
        }

        self.new_entries.allocator.free(self.save.data);
        self.save.data = new_data;

        self.save.data.len = new_len;
        for (self.new_entries.items, cursor..) |entry, it| {
            self.save.data[it] = .{
                .entry = entry,
                .is_active = true,
            };
        }
        self.new_entries.items.len = 0;
        self.save.inactive_entries = 0;
    }

    pub fn add_entry(self: *Save_Manager, entry: Data_Entry) !void {
        var new_entry: Data_Entry = undefined;
        new_entry.page_number = try self.allocator.dupe(u8, entry.page_number);
        new_entry.entry_title = try self.allocator.dupe(u8, entry.entry_title);
        new_entry.source_link = try self.allocator.dupe(u8, entry.source_link);
        new_entry.note = try self.allocator.dupe(u8, entry.note);
        new_entry.creation_date = try self.allocator.dupe(u8, entry.creation_date);
        new_entry.id = entry.id;
        new_entry.tags = try self.allocator.alloc([]u8, entry.tags.len);
        for (entry.tags, new_entry.tags) |old, *new| {
            new.* = try self.allocator.dupe(u8, old);
        }

        try self.new_entries.append(new_entry);
    }

    pub fn write_to_disk(self: Save_Manager, allocator: std.mem.Allocator) !void {
        const file = try std.fs.openFileAbsolute(self.file_path, .{.mode = .read_write});
        defer file.close();
        const writer = file.writer().any();
        var event_manager: Event_Manager = undefined;
        event_manager.events = std.ArrayList(Event).init(allocator);
        defer event_manager.events.deinit();
        try serialize(Save_Structure, self.save, writer, &event_manager);
    }

    pub fn load_from_disk(self: *Save_Manager) !void {
        const file = try std.fs.openFileAbsolute(self.file_path, .{.mode = .read_only});
        defer file.close();
        const reader = file.reader().any();
        self.save = try deserialize(Save_Structure, reader, self.allocator);
    }
};

var save_manager: Save_Manager = undefined;

const template_path = "template.temp";
const is_debug_build = true;

const Template_Field = enum {
    new_entry,
    new_entry_tag,
    new_entry_link,
    new_entry_page,
    form_separator,
    edit_form,
};


var templates: [std.meta.tags(Template_Field).len][]const u8 = undefined;
var static_allocator: std.mem.Allocator = undefined;

inline fn get_template(title: Template_Field) []const u8 {
    return templates[@intFromEnum(title)];
}

const ParseError = error {
    unrecognized_escape,
};


fn parse_json_string(str: []const u8, allocator: std.mem.Allocator) ![]const u8 {
    var buffer = std.ArrayList(u8).init(allocator);

    var is_escaped = false;
    for (str, 0..) |c, it_index| {
        if (is_escaped) {
            const escaped_char: u8 = switch (c) {
                'n' => '\n',
                'r' => '\r',
                't' => '\t',
                '"' => '"',
                '\\' => '\\',
                'b' => ' ',
                'B' => ' ',
                'u' => return ParseError.unrecognized_escape, // @todo unicode decoding
                else => {
                    std.debug.print(
                        "Unrecognized Escaped character: \\{}\nFound in\n------\n{s}\n------\n",
                        .{c, str[@max(0, it_index-10)..@min(it_index+10, str.len)]}
                    );
                    return ParseError.unrecognized_escape;
                },
            };
            try buffer.append(escaped_char);
            is_escaped = false;
        } else {
            if (c == '\\') {
                is_escaped = true;
            } else {
                try buffer.append(c);
            }
        }
    }
    return buffer.items;
}

fn data_entry_to_html(entry: Data_Entry, arena_allocator: std.mem.Allocator) ![]u8 {
    const full_template  = get_template(.new_entry);
    const tag_template   = get_template(.new_entry_tag);
    const title_template = get_template(.new_entry_link);
    const page_template  = get_template(.new_entry_page);

    var pieces_tags = std.ArrayList([]const u8).init(arena_allocator);
    
    for (entry.tags) |tag| {
        const tag_mapper = Mapper_Single{.key="tag", .value=&[_][]const u8{tag}};
        try replace(tag_template, tag_mapper, &pieces_tags);
        try pieces_tags.append("\n");
    }

    var pieces_title = std.ArrayList([]const u8).init(arena_allocator);
    if (entry.source_link.len > 0) {
        const title_mapper = Mapper_Slice {
            .data = &[_]Mapper_Slice.Item {
                .{.key = "entry_title", .value = &[_][]const u8{entry.entry_title}},
                .{.key = "source_link", .value = &[_][]const u8{entry.source_link}},
            },
        };
        try replace(title_template, title_mapper, &pieces_title);
    } else {
        try pieces_title.append(entry.entry_title);
    }

    var pieces_page = std.ArrayList([]const u8).init(arena_allocator);
    if (entry.page_number.len > 0) {
        const page_mapper = Mapper_Slice {
            .data = &[_]Mapper_Slice.Item {
                .{.key = "page_number", .value = &[_][]const u8{entry.page_number}},
            },
        };
        try replace(page_template, page_mapper, &pieces_page);
    }

    const id_text: []const u8 = &get_id(entry.id);

    const body_mapper = blk: {
        var map = std.StringHashMap([]const []const u8).init(arena_allocator);
        try map.put("note",          &[_][]const u8{entry.note});
        try map.put("creation_date", &[_][]const u8{entry.creation_date});
        try map.put("id",            &[_][]const u8{id_text});
        try map.put("pages", pieces_page.items);
        try map.put("tags",  pieces_tags.items);
        try map.put("title", pieces_title.items);

        break :blk Mapper_Hash {
            .map = map,
        };
    };
    var pieces_body = std.ArrayList([]const u8).init(arena_allocator);
    try replace(full_template, body_mapper, &pieces_body);

    const result = try std.mem.join(arena_allocator, "", pieces_body.items);

    return result;
}

const Api_Error = error {
    invalid_endpoint,
    invalid_id,
};

const Request = struct {
    target: []const u8,
    trigger_name: ?[]const u8 = null,
};


fn retrieve_id(trigger_name: ?[]const u8, prefix: []const u8) !struct{ [id_length]u8, u32 } {
        const parsed_id_whole = trigger_name orelse unreachable;
        if (builtin.mode == .Debug) {
            if (!std.mem.startsWith(u8, parsed_id_whole, prefix)) {
                std.log.err("Trigger Name is `{s}`, but it should start with `{s}`", .{parsed_id_whole, prefix});
                unreachable;
            }
        }
        const parsed_id = parsed_id_whole[prefix.len..];
        std.debug.assert(parsed_id.len == id_length);
        const parsed: [id_length]u8 = (@as(*const [id_length]u8, @ptrCast(parsed_id))).*;
        const id = parse_id(parsed);
        return .{ parsed, id, };
}

fn parse_json_to_entry(content: []const u8, arena_allocator: std.mem.Allocator) !Data_Entry {
    const data = try std.json.parseFromSliceLeaky(New_Entry_Data, arena_allocator, content, .{});

    const tags = blk: {
        var tag_array = std.ArrayList([]const u8).init(arena_allocator);
        var cursor_left: usize = 0;
        var cursor_right: usize = 0;
        for (data.input_tags) |c| {
            if (std.ascii.isWhitespace(c)) {
                if (cursor_left < cursor_right) {
                    try tag_array.append(data.input_tags[cursor_left..cursor_right]);
                }
                cursor_right += 1;
                cursor_left = cursor_right;
            } else {
                cursor_right += 1;
            }
        }
        if (cursor_left < cursor_right) {
            try tag_array.append(data.input_tags[cursor_left..cursor_right]);
        }
        break :blk tag_array.items;
    };

    const page_number = std.mem.trim( u8,
        try parse_json_string(data.page_number, arena_allocator),
        &std.ascii.whitespace,
    );
    const source_link = std.mem.trim( u8,
        try parse_json_string(data.input_source_link, arena_allocator),
        &std.ascii.whitespace,
    );

    const title = blk: {
        if (data.input_entry_title.len > 0) {
            break :blk try parse_json_string(data.input_entry_title, arena_allocator);
       } else {
           break :blk "No Title";
       }
    };

    const entry = Data_Entry {
        .page_number   = page_number,
        .entry_title   = title,
        .source_link   = source_link,
        .note          = try parse_json_string(data.input_note, arena_allocator),
        .tags          = tags,
        .creation_date = undefined,
        .id            = undefined,
    };
    return entry;
}

fn process_request(request: Request, content: []const u8, arena_allocator: std.mem.Allocator) ![]const u8 {
    const trimmed_target = std.mem.trim(u8, request.target, &std.ascii.whitespace);

    if (std.mem.eql(u8, trimmed_target, "/api/htmx/add_entry")) {

        const month_names = [12][]const u8 {
            "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"
        };
        const date = datetime.Date.now();
        const month = month_names[@intFromEnum(date.month)];
        const date_str = try std.fmt.allocPrint(arena_allocator, "{} {s} {}", .{date.day, month, date.year});

        save_manager.save.id_generator += 1;

        const entry = blk: {
            var e = try parse_json_to_entry(content, arena_allocator);
            e.id = @intCast(save_manager.save.id_generator);
            e.creation_date = date_str;
            break :blk e;
        };

        try save_manager.add_entry(entry);
        try save_manager.update_save();
        try save_manager.write_to_disk(arena_allocator);

        const separator_template = get_template(.form_separator);
        const result = try data_entry_to_html(entry, arena_allocator);
        const answer = try std.mem.join(arena_allocator, "\n", &[_][]const u8{separator_template, result});

        return answer;    
    } else if (std.mem.eql(u8, trimmed_target, "/api/htmx/get_history")) {
        var answer = std.ArrayList(u8).init(arena_allocator);
        const writer = answer.writer();
        var data_iter = std.mem.reverseIterator(save_manager.save.data);
        while (data_iter.next()) |entry| {
            const data = entry.entry;
            const html_data = try data_entry_to_html(data, arena_allocator);
            _ = try writer.write(html_data);
        }
        return answer.items;
    } else if (std.mem.eql(u8, trimmed_target, "/api/htmx/edit_entry")) {
        const body_template = get_template(.edit_form);

        const parsed_id, const id = try retrieve_id(request.trigger_name, "edit_button_");

        const entry = blk: {
            for (save_manager.save.data) |entry| {
                if (entry.entry.id == id) break :blk &entry;
            }
            std.log.err("Trying to edit non-existent id {s}", .{&parsed_id});
            return error.invalid_id;
        };

        const tags = try std.mem.join(arena_allocator, " ", entry.entry.tags);

        const body_mapper = Mapper_Slice {
            .data = &[_] Mapper_Slice.Item {
                .{ .key="title", .value= &[_][]const u8 {entry.entry.entry_title}},
                .{ .key="id",    .value= &[_][]const u8 {&parsed_id}},
                .{ .key="link",  .value= &[_][]const u8 {entry.entry.source_link}},
                .{ .key="note",  .value= &[_][]const u8 {entry.entry.note}},
                .{ .key="pages", .value= &[_][]const u8 {entry.entry.page_number}},
                .{ .key="tags",  .value= &[_][]const u8 {tags}},
            },
        };
        var body_pieces = std.ArrayList([]const u8).init(arena_allocator);
        try replace(body_template, body_mapper, &body_pieces);

        const result = try std.mem.join(arena_allocator, "", body_pieces.items);
        return result;
    } else if (std.mem.eql(u8, trimmed_target, "/api/htmx/edit_cancel")) {
        const parsed_id, const id = try retrieve_id(request.trigger_name, "cancel_button_");
        _ = parsed_id;

        const entry = for (save_manager.save.data) |e| {
            if (e.entry.id == id) break e.entry;
        } else {
            return error.invalid_id;
        };

        return data_entry_to_html(entry, arena_allocator);
    } else if (std.mem.eql(u8, trimmed_target, "/api/htmx/edit_confirm")) {
        const parsed_id, const id = try retrieve_id(request.trigger_name, "form_");
        _ = parsed_id;

        const entry = for (save_manager.save.data) |*e| {
            if (e.entry.id == id) {
                e.is_active = false;
                save_manager.save.inactive_entries += 1;
                break e.entry;
            }
        } else {
            return error.invalid_id;
        };
        const new_entry = blk: {
            var e = try parse_json_to_entry(content, arena_allocator);
            e.id = entry.id;
            e.creation_date = entry.creation_date;
            break :blk e;
        };

        try save_manager.add_entry(new_entry);
        try save_manager.update_save();
        try save_manager.write_to_disk(arena_allocator);

        const answer = try data_entry_to_html(new_entry, arena_allocator);

        return answer; 

    } else if (std.mem.eql(u8, trimmed_target, "/api/htmx/edit_delete")) {
        const parsed_id, const id = try retrieve_id(request.trigger_name, "delete_button_");
        _ = parsed_id;

        for (save_manager.save.data) |*e| {
            if (e.entry.id == id) {
                e.is_active = false;
                save_manager.save.inactive_entries += 1;
                break;
            }
        } else {
            return error.invalid_id;
        }

        try save_manager.update_save();
        try save_manager.write_to_disk(arena_allocator);
        return "";
    }
    return error.invalid_endpoint;
}

pub fn main() !void {

    // @@ init vars
    
    var static_arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    static_allocator = static_arena.allocator();

    var _gpa_allocator = std.heap.GeneralPurposeAllocator(.{.thread_safe=false}) {};
    const gpa_allocator = _gpa_allocator.allocator();

    {
        var _arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
        defer _arena.deinit();
        const _allocator = _arena.allocator();

        const path = blk: {
            var buffer: [512]u8 = undefined;
            const exe_path = try std.fs.selfExeDirPath(&buffer);
            break :blk try std.fs.path.join(_allocator, &[_][]const u8{exe_path, template_path});
        };

        const template_file = try std.fs.openFileAbsolute(path, .{});
        const template_content = try template_file.reader().readAllAlloc(_allocator, std.math.maxInt(usize));

        var template_hash = std.StringHashMap([]const u8).init(_allocator);

        var temp_iterator = std.mem.splitSequence(u8, template_content, "@@@");
        while (temp_iterator.next()) |temp| {
            const mid = std.mem.indexOf(u8, temp, "\n") orelse continue;
            const title = std.mem.trim(u8, temp[0..mid], &std.ascii.whitespace);
            const value = std.mem.trim(u8, temp[mid..temp.len], &std.ascii.whitespace);
            try template_hash.put(title, value);
        }

        const declared_enums = @typeInfo(Template_Field).Enum.fields;

        if (is_debug_build) {
            if (declared_enums.len > template_hash.count()) {
                inline for (declared_enums) |s| {
                    if (!template_hash.contains(s.name)) {
                        std.debug.panic("Field `{s}` is not found in template enum!", .{s.name});
                    }
                }
            }
            var iter = template_hash.iterator();
            while (iter.next()) |entry| {
                const t = entry.key_ptr.*;
                inline for (declared_enums) |s| {
                    if (std.mem.eql(u8, t, s.name)) break;
                } else {
                    std.debug.panic("Field `{s}` is not found in template!", .{t});
                }
            }
        }

        inline for (declared_enums) |field| {
            const value = template_hash.get(field.name) orelse unreachable;
            templates[field.value] = try static_allocator.dupe(u8, value);
        }

        {
            var buffer: [256]u8 = undefined;
            const base_path = try std.fs.selfExeDirPath(@ptrCast(&buffer));
            const save_path = try std.fs.path.join(static_allocator, &[_][]const u8{base_path, save_file});
            save_manager = .{
                .file_path = save_path,
                .new_entries = std.ArrayList(Data_Entry).init(gpa_allocator),
                .save = undefined,
                .allocator = gpa_allocator,
            };

            const file_err = std.fs.openFileAbsolute(save_manager.file_path, .{});
            if (!std.meta.isError(file_err)) {
                try save_manager.load_from_disk();
            } else {
                save_manager.save = .{
                    .id_generator     = magic_id_number,
                    .footer_pointer   = undefined,
                    .inactive_entries = 0,
                    ._reserved_space  = std.mem.zeroes([104]u8),
                    .data = &.{},
                };
                const new_file = try std.fs.createFileAbsolute(save_manager.file_path, .{});
                new_file.close();
                try save_manager.write_to_disk(_allocator);
                try save_manager.load_from_disk();
            }
        }
    }



    // @@ server go

    const address = std.net.Address.initIp4([4]u8{127, 0, 0, 1}, 8081);
    var server = try address.listen(.{.reuse_address=true,}); 
    defer server.deinit();

    const buffer: []u8 = @ptrCast(@constCast(&std.mem.zeroes([4096]u8)));

    var arena = std.heap.ArenaAllocator.init(gpa_allocator);
    const arena_allocator = arena.allocator();

    while (true) {
        defer _ = arena.reset(.retain_capacity);

        const connection = try server.accept();
        defer connection.stream.close();

        var http_server = std.http.Server.init(connection, buffer);
        var http_request = try http_server.receiveHead();

        var reader = try http_request.reader();
        const content = try reader.readAllAlloc(arena_allocator, std.math.maxInt(usize));

        var request = Request {
            .target = http_request.head.target, 
        };

        var headers = http_request.iterateHeaders();
        while (headers.next()) |header| {
            // std.debug.print("{s}\n", .{header.name});
            if (std.mem.eql(u8, header.name, "HX-Trigger-Name")) {
                request.trigger_name = header.value;
            }
        }

        const answer = try process_request(request, content, arena_allocator);
        try http_request.respond(answer, .{});
    }
}
