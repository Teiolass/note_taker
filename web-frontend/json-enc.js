htmx.defineExtension('json-enc', {
    onEvent: function (name, evt) {
        if (name === "htmx:configRequest") {
            evt.detail.headers['Content-Type'] = "application/json";
        }
    },
    
    encodeParameters : function(xhr, parameters, elt) {
        xhr.overrideMimeType('text/json');
        for (var prop in parameters) {
            if (Object.prototype.hasOwnProperty.call(parameters, prop)) {
                if (typeof(parameters[prop]) == 'string') {
                    parameters[prop] = parameters[prop].replaceAll('\\', '\\\\');
                }
            }
        }
        return (JSON.stringify(parameters));
    }
});
